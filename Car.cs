﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___27
{
    public class Car
    {
        public Car(string brand, bool totalLost, bool needsRepair)
        {
            Brand = brand;
            TotalLost = totalLost;
            NeedsRepair = needsRepair;
            if (TotalLost is true && NeedsRepair is false)
                throw new RepairMismatchException("a car cant be a total lost without needing repair");
        }
        
        public string Brand { get; private set; }
        public bool TotalLost { get; private set; }
        public bool NeedsRepair { get; set; }// i cant make it private set cus i need to change the NeedsRepair in the FixCar function in the garage

        public override string ToString()
        {
            return $"brand: {Brand}, is total lost? {TotalLost}, needs repair? {NeedsRepair}";
        }
    }
}
