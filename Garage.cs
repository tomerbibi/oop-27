﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___27
{
    public class Garage
    {
        private List<Car> _cars = new List<Car>();
        private List<string> _carTypes;

        public Garage(List<string> carTypes)
        {
            _carTypes = carTypes;
        }
        public void AddCar(Car c)
        {
            if (c is null)
                throw new CarNullException();
            if (_cars.Contains(c))
                throw new CarAlreadyHereException($"the car {c} clready is already in the garage");
            if (c.TotalLost)
                throw new WeDoNotFixTotalLostException($"the car {c} cant be fixed");
            if (!_carTypes.Contains(c.Brand))
                throw new WrongGarageException($"that garage cant deal with cars of the brand {c.Brand}");
            if (!c.NeedsRepair)
                throw new RepairMismatchException($"the car {c} does not need to be repaired");
            _cars.Add(c);
        }
        public void TakeOutCar(Car c)
        {
            // there is no need to check if the car is null because if we add a null car to the garage it throws
            // an exception and doesnt add it to the garage
            if (!_cars.Contains(c))
                throw new CarNotInGarageException($"the car {c} is not in that garage");
            if (c.NeedsRepair)
                throw new CarNotReadyException($"the car {c} still needs to be repaired");
            _cars.Remove(c);
        }
        public void FixCar(Car c)
        {
            // there is no need to check if the car is null because if we add a null car to the garage it throws
            // an exception and doesnt add it to the garage
            if (!_cars.Contains(c))
                throw new CarNotInGarageException($"the car {c} is not in that garage");
            // there is no need to check if the doesnt need repair because if we add a car that doesnt need
            // repair to the garage it throws an exceprion and doesnt add it to the garage
        }
    }
}
