﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using oop___27;
using System.Collections.Generic;

namespace GarageTest
{
    [TestClass]
    public class GarageTest
    {

        [TestMethod]
        [ExpectedException(typeof(CarAlreadyHereException))]
        public void Garage_AddCarFunction_IsTheCarAlreadyThere()
        {
            Car c = new Car("honda", false, true);
            List<string> s = new List<string>
            {
                "honda"
            };
            Garage g = new Garage(s);
            g.AddCar(c);
            g.AddCar(c);
        }
        [TestMethod]
        [ExpectedException(typeof(WeDoNotFixTotalLostException))]
        public void Garage_AddCarFunction_IsCarTotalLots()
        {
            List<string> s = new List<string>
            {
                "honda"
            };
            Garage g = new Garage(s);
            Car car = new Car("honda", true, true);
            g.AddCar(car);
        }
        [TestMethod]
        [ExpectedException(typeof(WrongGarageException))]
        public void Garage_AddCarFunction_CanGarageHandleType()
        {
            Car c = new Car("suzuki", false, true);
            List<string> s = new List<string>
            {
                "honda"
            };
            Garage g = new Garage(s);
            g.AddCar(c);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNullException))]
        public void Garage_AddCarFunction_IsItNull()
        {
            Car c = null;
            List<string> s = new List<string>
            {
                "honda"
            };
            Garage g = new Garage(s);
            g.AddCar(c);
        }
        [TestMethod]
        [ExpectedException(typeof(RepairMismatchException))]
        public void Garage_AddCarFunction_DoesCarNeedRepair()
        {
            Car c = new Car("honda", false, false);
            List<string> s = new List<string>
            {
                "honda"
            };
            Garage g = new Garage(s);
            g.AddCar(c);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNotInGarageException))]
        public void Garage_TakeOutCarFunction_IscarInTheGarage()
        {
            Car c = new Car("honda", false, true);
            List<string> s = new List<string>
            {
                "honda"
            };
            Garage g = new Garage(s);
            g.TakeOutCar(c);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNotReadyException))]
        public void Garage_TakeOutCarFunction_IsCarFixed()
        {
            Car c = new Car("honda", false, true);
            List<string> s = new List<string>
            {
                "honda"
            };
            Garage g = new Garage(s);
            g.AddCar(c);
            g.TakeOutCar(c);
        }
        [TestMethod]
        [ExpectedException(typeof(CarNotInGarageException))]
        public void Garage_FixCarFunction_IscarInTheGarage()
        {
            Car c = new Car("honda", false, true);
            List<string> s = new List<string>
            {
                "honda"
            };
            Garage g = new Garage(s);
            g.FixCar(c);
        }
    }
}
